﻿namespace WaveformGenerator
{
	partial class MainForm
	{
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{
			this.ListDataGridView = new System.Windows.Forms.DataGridView();
			this.label1 = new System.Windows.Forms.Label();
			this.Total_TextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.Undefined_TextBox = new System.Windows.Forms.TextBox();
			this.Save_Button = new System.Windows.Forms.Button();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.High_TextBox = new System.Windows.Forms.TextBox();
			this.Low_TextBox = new System.Windows.Forms.TextBox();
			this.Add_Button = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.Times_TextBox = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.Start_TextBox = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.ListDataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// ListDataGridView
			// 
			this.ListDataGridView.AllowUserToAddRows = false;
			this.ListDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.ListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.ListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
			this.ListDataGridView.Location = new System.Drawing.Point(0, 65);
			this.ListDataGridView.Name = "ListDataGridView";
			this.ListDataGridView.RowTemplate.Height = 21;
			this.ListDataGridView.Size = new System.Drawing.Size(592, 444);
			this.ListDataGridView.TabIndex = 9;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(29, 12);
			this.label1.TabIndex = 1;
			this.label1.Text = "総数";
			// 
			// Total_TextBox
			// 
			this.Total_TextBox.Location = new System.Drawing.Point(47, 6);
			this.Total_TextBox.Name = "Total_TextBox";
			this.Total_TextBox.Size = new System.Drawing.Size(71, 19);
			this.Total_TextBox.TabIndex = 1;
			this.Total_TextBox.Text = "10000";
			this.Total_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(124, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(53, 12);
			this.label2.TabIndex = 3;
			this.label2.Text = "未指定時";
			// 
			// Undefined_TextBox
			// 
			this.Undefined_TextBox.Location = new System.Drawing.Point(179, 6);
			this.Undefined_TextBox.Name = "Undefined_TextBox";
			this.Undefined_TextBox.Size = new System.Drawing.Size(75, 19);
			this.Undefined_TextBox.TabIndex = 2;
			this.Undefined_TextBox.Text = "0";
			this.Undefined_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// Save_Button
			// 
			this.Save_Button.Location = new System.Drawing.Point(464, 6);
			this.Save_Button.Name = "Save_Button";
			this.Save_Button.Size = new System.Drawing.Size(116, 53);
			this.Save_Button.TabIndex = 8;
			this.Save_Button.Text = "出力";
			this.Save_Button.UseVisualStyleBackColor = true;
			this.Save_Button.Click += new System.EventHandler(this.Save_Button_Click);
			// 
			// Column1
			// 
			this.Column1.HeaderText = "H時間";
			this.Column1.Name = "Column1";
			// 
			// Column2
			// 
			this.Column2.HeaderText = "L時間";
			this.Column2.Name = "Column2";
			// 
			// Column3
			// 
			this.Column3.HeaderText = "回数";
			this.Column3.Name = "Column3";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 38);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 12);
			this.label3.TabIndex = 6;
			this.label3.Text = "High回数";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(124, 38);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(49, 12);
			this.label4.TabIndex = 6;
			this.label4.Text = "Low回数";
			// 
			// High_TextBox
			// 
			this.High_TextBox.Location = new System.Drawing.Point(70, 35);
			this.High_TextBox.Name = "High_TextBox";
			this.High_TextBox.Size = new System.Drawing.Size(48, 19);
			this.High_TextBox.TabIndex = 4;
			this.High_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// Low_TextBox
			// 
			this.Low_TextBox.Location = new System.Drawing.Point(179, 35);
			this.Low_TextBox.Name = "Low_TextBox";
			this.Low_TextBox.Size = new System.Drawing.Size(51, 19);
			this.Low_TextBox.TabIndex = 5;
			this.Low_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// Add_Button
			// 
			this.Add_Button.Location = new System.Drawing.Point(339, 33);
			this.Add_Button.Name = "Add_Button";
			this.Add_Button.Size = new System.Drawing.Size(119, 23);
			this.Add_Button.TabIndex = 7;
			this.Add_Button.Text = "追加";
			this.Add_Button.UseVisualStyleBackColor = true;
			this.Add_Button.Click += new System.EventHandler(this.Add_Button_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(237, 38);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(29, 12);
			this.label5.TabIndex = 6;
			this.label5.Text = "回数";
			// 
			// Times_TextBox
			// 
			this.Times_TextBox.Location = new System.Drawing.Point(272, 35);
			this.Times_TextBox.Name = "Times_TextBox";
			this.Times_TextBox.Size = new System.Drawing.Size(61, 19);
			this.Times_TextBox.TabIndex = 6;
			this.Times_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(260, 9);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(65, 12);
			this.label6.TabIndex = 3;
			this.label6.Text = "開始時回数";
			// 
			// Start_TextBox
			// 
			this.Start_TextBox.Location = new System.Drawing.Point(331, 6);
			this.Start_TextBox.Name = "Start_TextBox";
			this.Start_TextBox.Size = new System.Drawing.Size(61, 19);
			this.Start_TextBox.TabIndex = 3;
			this.Start_TextBox.Text = "10";
			this.Start_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(592, 508);
			this.Controls.Add(this.Add_Button);
			this.Controls.Add(this.Start_TextBox);
			this.Controls.Add(this.Times_TextBox);
			this.Controls.Add(this.Low_TextBox);
			this.Controls.Add(this.High_TextBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.Save_Button);
			this.Controls.Add(this.Undefined_TextBox);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.Total_TextBox);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.ListDataGridView);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.Text = "Waveform Generator";
			((System.ComponentModel.ISupportInitialize)(this.ListDataGridView)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView ListDataGridView;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox Total_TextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox Undefined_TextBox;
		private System.Windows.Forms.Button Save_Button;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
		private System.Windows.Forms.SaveFileDialog _SaveFileDialog;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox High_TextBox;
		private System.Windows.Forms.TextBox Low_TextBox;
		private System.Windows.Forms.Button Add_Button;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox Times_TextBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox Start_TextBox;
	}
}

