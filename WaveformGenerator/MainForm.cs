﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WaveformGenerator
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void Save_Button_Click(object sender, EventArgs e)
		{
			if (_SaveFileDialog.ShowDialog() == DialogResult.OK)
			{
				try
				{
					string out_text = "";
					var total = Convert.ToInt32(Total_TextBox.Text);
					var undef = Convert.ToInt32(Undefined_TextBox.Text);
					var start = Convert.ToInt32(Start_TextBox.Text);

					try
					{
						for (int i = 0; i < start; ++i)
						{
							out_text += undef.ToString() + "\r\n";
							--total;
						}
						foreach (var row in ListDataGridView.Rows.Cast<DataGridViewRow>())
						{
							var h_times = Convert.ToInt32(row.Cells[0].Value.ToString());
							var l_times = Convert.ToInt32(row.Cells[1].Value.ToString());
							var times = Convert.ToInt32(row.Cells[2].Value.ToString());
							for (int i = 0; i < times; ++i)
							{
								for (int j = 0; j < h_times; ++j)
								{
									out_text += "1\r\n";
									--total;
								}
								for (int j = 0; j < l_times; ++j)
								{
									out_text += "0\r\n";
									--total;
								}
							}
						}
						for (int i = 0; i < total; ++i)
						{
							out_text += undef.ToString() + "\r\n";
						}
					}
					catch (Exception ex)
					{
						System.Diagnostics.Debug.WriteLine(ex.Message);
						MessageBox.Show("入力リストの変換に失敗しました。", "エラー");
						return;
					}

					try
					{
						using (System.IO.StreamWriter outputFile = new System.IO.StreamWriter(_SaveFileDialog.FileName, false))
						{
							outputFile.Write(out_text);
						}
					}
					catch (Exception ex)
					{
						System.Diagnostics.Debug.WriteLine(ex.Message);
						MessageBox.Show("ファイル書き込みに失敗しました。", "エラー");
						return;
					}
				}
				catch (Exception ex)
				{
					System.Diagnostics.Debug.WriteLine(ex.Message);
					MessageBox.Show("入力テキストボックスの値が整数ではありません。", "エラー");
					return;
				}
			}
		}

		private void Add_Button_Click(object sender, EventArgs e)
		{
			ListDataGridView.Rows.Add(new object[] { High_TextBox.Text, Low_TextBox.Text, Times_TextBox.Text });
		}
	}
}
